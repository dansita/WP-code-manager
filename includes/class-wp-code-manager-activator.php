<?php

/**
 * Fired during plugin activation
 *
 * @link       dansedmak.com
 * @since      1.0.0
 *
 * @package    Wp_Code_Manager
 * @subpackage Wp_Code_Manager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Code_Manager
 * @subpackage Wp_Code_Manager/includes
 * @author     Daniel Sedmak <mail@dansedmak.com>
 */
class Wp_Code_Manager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
